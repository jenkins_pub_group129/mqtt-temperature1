# My Iot on Docker Swarm Cluster
App to plot in realtime IoT temperature sensor

By: 2022-12-16 | César Bento Freire

## Project scope

* ESP32 + DHT22
* Mosquitto broker with MQTT Protocol
* Full Python project

## GitLab

* Create project
* Clone project on VSCODE
* Add python files
* Add project integration to jenkins
* Create a user token with API


## Docker playground

* Create 3 Managers + 2 Works with template
* Clone repository

__deploy__
    
    docker stack deploy --compose-file docker-compose.yml app
    docker stack ps app
    
## Jenkins

* Add plugin gitlab
* Configure GitLab plugin
* Add plugin Publish Over SSH
* Create pipeline project
* 