# Rest Server

WEB REST API from redis and mongo server


## Build

    docker build -t goncalopmatias/rest-server .
    
## Run

    docker run -d -p 8080:8000 --name rest-server --link redis --link mongo goncalopmatias/rest-server