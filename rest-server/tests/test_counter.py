import pytest

import counter as cnt

def test_counter():
    c = cnt.Counter()
    assert c.counter == 1
    c.counter = 10
    assert c.counter == 11
    assert c is not None