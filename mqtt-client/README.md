# Mqtt Client

Client of mosquitto MQTT Protocol with history on mongo and cache on redis

## Dependencies

### Mongo
    docker run -itd --name mongo -p 27017:27017 --restart on-failure -v mongo_database:/data/db mongo

### Redis
    docker run -itd --name redis -p 6379:6379/tcp --restart on-failure redis:alpine

### mqtt_client

**build**

    docker build -t goncalopmatias/mqtt-client .

**run**

   docker run -d --link redis --link mongo --name mqtt-client goncalopmatias/mqtt-client

## Check running

    docker logs mqtt-client -f
    docker exec -it redis sh
    docker exec -it mongo sh